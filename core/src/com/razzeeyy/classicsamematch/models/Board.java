package com.razzeeyy.classicsamematch.models;

import com.badlogic.gdx.ai.fsm.DefaultStateMachine;
import com.badlogic.gdx.ai.fsm.StateMachine;
import com.badlogic.gdx.ai.msg.MessageDispatcher;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;
import com.razzeeyy.classicsamematch.GameConstants;
import com.razzeeyy.classicsamematch.models.states.AbstractBoardState;
import com.razzeeyy.classicsamematch.models.states.FallBoardState;
import com.razzeeyy.classicsamematch.models.states.GameOverBoardState;
import com.razzeeyy.classicsamematch.models.states.GameStartBoardState;
import com.razzeeyy.classicsamematch.models.states.IdleBoardState;
import com.razzeeyy.classicsamematch.models.states.MatchBoardState;
import com.razzeeyy.classicsamematch.models.states.MoveCheckBoardState;
import com.razzeeyy.classicsamematch.models.states.RepopulateBoardState;
import com.razzeeyy.classicsamematch.models.states.SlideBoardState;
import com.razzeeyy.classicsamematch.models.states.UndoBoardState;
import com.razzeeyy.classicsamematch.utils.Array2;

/**
 * Board model, handles the logical game state of the board with tiles,
 * and emits various change events when some logical transitions happen.
 * Created by razzeeyy on 21.01.17.
 */

public class Board extends Actor {
    public final Array2<Color> tiles;

    private final AbstractBoardState STATE_GAMESTART = new GameStartBoardState();
    public final AbstractBoardState STATE_IDLE = new IdleBoardState();
    public final AbstractBoardState STATE_MATCH = new MatchBoardState();
    public final AbstractBoardState STATE_FALL = new FallBoardState();
    public final AbstractBoardState STATE_SLIDE = new SlideBoardState();
    public final AbstractBoardState STATE_MOVECHECK = new MoveCheckBoardState();
    public final AbstractBoardState STATE_GAMEOVER = new GameOverBoardState();
    public final AbstractBoardState STATE_UNDO = new UndoBoardState();
    private final AbstractBoardState STATE_REPOPULATE = new RepopulateBoardState();

    public final StateMachine<Board, AbstractBoardState> stateMachine = new DefaultStateMachine<Board, AbstractBoardState>(this);

    public final Vector2 INPUT_NONE = new Vector2(-1, -1);
    public final Vector2 input = new Vector2(INPUT_NONE);

    public final MessageDispatcher messageDispatcher;
    public final Color[] colors;

    public final Pool<Move> movePool = Pools.get(Move.class);
    public final Array<Move> moveHistory = new Array<Move>();

    public boolean undoRequested = false;

    public Board(MessageDispatcher messageDispatcher, int width, int height, Color[] colors) {
        this.messageDispatcher = messageDispatcher;
        this.colors = colors;
        tiles = new Array2<Color>(width, height);
    }

    @Override
    public void act(float delta) {
        stateMachine.update();
    }

    public void populate() {
        stateMachine.changeState(STATE_GAMESTART);
    }

    public void match(int x, int y) {
        input.set(x, y);
    }

    public void undo() {
        undoRequested = true;
    }

    public void addTile(int x, int y, Color color) {
        tiles.set(x, y, color);

        Move moveAdd = movePool.obtain();
        moveAdd.actionAdd(x, y, color);

        messageDispatcher.dispatchMessage(GameConstants.EVENT_ADD, moveAdd);

        movePool.free(moveAdd);
    }

    public void removeTile(int x, int y) {
        Color color = tiles.get(x, y);
        tiles.set(x, y, null);

        Move moveRemove = movePool.obtain();
        moveRemove.actionRemove(x, y, color);

        messageDispatcher.dispatchMessage(GameConstants.EVENT_REMOVE, moveRemove);

        moveHistory.add(moveRemove);
    }

    public void moveTile(int fromX, int fromY, int toX, int toY) {
        Color tile = tiles.get(fromX, fromY);

        tiles.set(fromX, fromY, null);
        tiles.set(toX, toY, tile);

        Move moveMove = movePool.obtain();
        moveMove.actionMove(fromX, fromY, toX, toY);

        messageDispatcher.dispatchMessage(GameConstants.EVENT_MOVE, moveMove);

        if (stateMachine.getCurrentState() != STATE_UNDO) {
            moveHistory.add(moveMove);
        } else {
            movePool.free(moveMove);
        }
    }

    public void clearMoveHistory() {
        while(moveHistory.size > 0) {
            Move move = moveHistory.pop();
            movePool.free(move);
        }
    }

    public void repopulate() {
        stateMachine.changeState(STATE_REPOPULATE);
    }
}
