package com.razzeeyy.classicsamematch.models;

import com.badlogic.gdx.graphics.Color;
import com.razzeeyy.classicsamematch.GameConstants;

/**
 * Basically a command/event class for relaying events about moves done or keeping history of moves for future undo
 * Created by razze on 21.01.2017.
 */

public class Move {
    private static final int MATCH = GameConstants.EVENT_MATCH;
    public static final int ADD = GameConstants.EVENT_ADD;
    private static final int REMOVE = GameConstants.EVENT_REMOVE;
    public static final int MOVE = GameConstants.EVENT_MOVE;

    private int id;
    private int x;
    private int y;
    private int toX;
    private int toY;
    private Color color;

    public Move() {

    }

    private void initialize(int id, int x, int y, int toX, int toY, Color color) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.toX = toX;
        this.toY = toY;
        this.color = color;
    }

    public void actionAdd(int x, int y, Color color) {
        initialize(ADD, x, y, 0, 0, color);
    }

    public void actionRemove(int x, int y, Color color) {
        initialize(REMOVE, x, y, 0, 0, color);
    }

    public void actionMove(int x, int y, int toX, int toY) {
        initialize(MOVE, x, y, toX, toY, null);
    }

    public void actionMatch(int x, int y, int matchLength, boolean isSuccessfulMatch) {
        initialize(MATCH, x, y, matchLength, isSuccessfulMatch ? 1 : 0, null);
    }

    public int getId() {
        return id;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getToX() {
        return toX;
    }

    public int getToY() {
        return toY;
    }

    public Color getColor() {
        return color;
    }

    public int getMatchLength() {
        return toX;
    }

    public boolean isSuccessfulMatch() {
        return toY == 1;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Move) {
            Move other = (Move) obj;
            return other.id == id &&
                    other.x == x &&
                    other.y == y &&
                    other.toX == toX &&
                    other.toY == toY &&
                    other.color == color;
        }
        return false;
    }

    public void copy(Move other) {
        id = other.id;
        x = other.x;
        y = other.y;
        toX = other.toX;
        toY = other.toY;
        color = other.color;
    }

    public void reverse() {
        switch (id) {
            case ADD:
                id = REMOVE;
                break;
            case REMOVE:
                id = ADD;
                break;
            case MOVE:
                final int tmpX = x;
                final int tmpY = y;

                x = toX;
                y = toY;

                toX = tmpX;
                toY = tmpY;
                break;
        }
    }

    public boolean isFall() {
        return Math.abs(x - toX) < Math.abs(y - toY);
    }

    public boolean isSlide() {
        return Math.abs(x - toX) > Math.abs(y - toY);
    }

    public boolean isMatch() {
        return id == MATCH;
    }
}
