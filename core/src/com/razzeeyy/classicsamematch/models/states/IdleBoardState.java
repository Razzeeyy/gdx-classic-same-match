package com.razzeeyy.classicsamematch.models.states;

import com.razzeeyy.classicsamematch.models.Board;

/**
 * When the board model has nothing to do it keeps checking if there is no input or undo request
 * Created by razzeeyy on 21.01.17.
 */

public class IdleBoardState extends AbstractBoardState {
    @Override
    public void update(Board board) {
        if(board.undoRequested) {
            board.undoRequested = false;
            board.input.set(board.INPUT_NONE);
            board.stateMachine.changeState(board.STATE_UNDO);
            return;
        }

        if(!board.input.epsilonEquals(board.INPUT_NONE, 0)) {
            board.stateMachine.changeState(board.STATE_MATCH);
        }
    }
}
