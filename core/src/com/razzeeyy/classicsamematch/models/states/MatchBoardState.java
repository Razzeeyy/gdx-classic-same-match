package com.razzeeyy.classicsamematch.models.states;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;
import com.razzeeyy.classicsamematch.GameConstants;
import com.razzeeyy.classicsamematch.models.Board;
import com.razzeeyy.classicsamematch.models.Move;
import com.razzeeyy.classicsamematch.utils.Array2;

/**
 * when input is provided to board model it handles finding a match on the input and removing it if any
 * Created by razzeeyy on 21.01.17.
 */

public class MatchBoardState extends AbstractBoardState {
    private final Pool<Vector2> vectorPool = Pools.get(Vector2.class);
    private final Array<Vector2> tmp = new Array<Vector2>();

    @Override
    public void update(Board board) {
        Vector2 currentInput = board.input;
        int x = (int) currentInput.x;
        int y = (int) currentInput.y;

        tmp.clear();
        Array<Vector2> match = floodfill(board.tiles, x, y, tmp, null);

        final boolean isMatchSuccessful = match.size >= 2;

        Move moveMatch = board.movePool.obtain();
        moveMatch.actionMatch(x, y, match.size, isMatchSuccessful);

        board.messageDispatcher.dispatchMessage(GameConstants.EVENT_MATCH, moveMatch);

        if (isMatchSuccessful) {
            board.clearMoveHistory(); //only one match can be undone

            board.moveHistory.add(moveMatch); //add only successfull matches to the history

            for (Vector2 position : match) {
                board.removeTile((int) position.x, (int) position.y);
                vectorPool.free(position);
            }
        }

        board.input.set(board.INPUT_NONE);

        board.stateMachine.changeState(board.STATE_FALL);
    }

    private Array<Vector2> floodfill(Array2<Color> tiles, int x, int y, Array<Vector2> result, Color color) {
        if (!tiles.contains(x, y)) return result;

        final Color tile = tiles.get(x, y);

        if (tile == null) return result;

        if (color == null) color = tile;

        final Vector2 position = vectorPool.obtain();
        position.set(x, y);

        if (tile == color && !result.contains(position, false)) {
            result.add(position);
            floodfill(tiles, x + 1, y, result, color);
            floodfill(tiles, x - 1, y, result, color);
            floodfill(tiles, x, y + 1, result, color);
            floodfill(tiles, x, y - 1, result, color);
        }

        return result;
    }
}
