package com.razzeeyy.classicsamematch.models.states;

import com.badlogic.gdx.graphics.Color;
import com.razzeeyy.classicsamematch.models.Board;
import com.razzeeyy.classicsamematch.utils.Array2;

import java.util.Random;

/**
 * Used for self playing board view in the menu to restart itself without reinstantiating the whole class
 * Created by razzeeyy on 21.01.17.
 */

public class RepopulateBoardState extends AbstractBoardState {
    @Override
    public void update(Board board) {
        final Random random = new Random();
        final Array2<Color> tiles = board.tiles;
        final Color[] colors = board.colors;

        for (int x = 0; x < tiles.width; x++) {
            for (int y = 0; y < tiles.height; y++) {
                if(tiles.get(x, y) == null) board.addTile(x, y, colors[random.nextInt(colors.length)]);
            }
        }

        board.stateMachine.changeState(board.STATE_IDLE);
    }
}
