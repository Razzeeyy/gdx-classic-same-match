package com.razzeeyy.classicsamematch.models.states;

import com.badlogic.gdx.ai.fsm.State;
import com.badlogic.gdx.ai.msg.Telegram;
import com.razzeeyy.classicsamematch.models.Board;

/**
 * Base board state, here just for completeness sake
 * and for future proofness if any base functionality will need to be added to all states
 * Created by razzeeyy on 21.01.17.
 */

public abstract class AbstractBoardState implements State<Board> {
    @Override
    public void enter(Board board) {

    }

    @Override
    public void update(Board board) {

    }

    @Override
    public void exit(Board board) {

    }

    @Override
    public boolean onMessage(Board board, Telegram telegram) {
        return false;
    }
}
