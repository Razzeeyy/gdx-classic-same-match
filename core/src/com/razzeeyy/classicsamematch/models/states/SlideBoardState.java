package com.razzeeyy.classicsamematch.models.states;

import com.badlogic.gdx.graphics.Color;
import com.razzeeyy.classicsamematch.GameConstants;
import com.razzeeyy.classicsamematch.models.Board;
import com.razzeeyy.classicsamematch.utils.Array2;

/**
 * if there is a whole blank columns in the map slides columns on the left to the right to close this gap
 * Created by razzeeyy on 21.01.17.
 */

public class SlideBoardState extends AbstractBoardState {
    @Override
    public void update(Board board) {
        Array2<Color> tiles = board.tiles;

        int gapX = -1;
        int wholeX = -1;

        for (int x = 0; x < tiles.width && gapX == -1; x++) {
            if (tiles.get(x, 0) == null) {
                gapX = x;
                for (int sideX = x + 1; sideX < tiles.width; sideX++) {
                    if (tiles.get(sideX, 0) != null) {
                        wholeX = sideX;
                        break;
                    }
                }
            }
        }

        if (gapX != -1 && wholeX != -1) {
            board.messageDispatcher.dispatchMessage(GameConstants.EVENT_SLIDE);

            int distance = wholeX - gapX;

            for (int x = wholeX; x < tiles.width; x++) {
                for (int y = 0; y < tiles.height; y++) {
                    Color tile = tiles.get(x, y);

                    if (tile != null) {
                        board.moveTile(x, y, x - distance, y);
                    }
                }
            }
        }

        board.stateMachine.changeState(board.STATE_MOVECHECK);
    }
}
