package com.razzeeyy.classicsamematch.models.states;

import com.badlogic.gdx.graphics.Color;
import com.razzeeyy.classicsamematch.GameConstants;
import com.razzeeyy.classicsamematch.models.Board;

/**
 * When board has no more tiles to match it enters this state to prevent further input and notify of the game end.
 * Created by razzeeyy on 21.01.17.
 */

public class GameOverBoardState extends AbstractBoardState {
    @Override
    public void enter(Board board) {
        Boolean boardCleared = Boolean.TRUE;

        for (int x = 0; x < board.tiles.width && boardCleared; x++) {
            for (int y = 0; y < board.tiles.height && boardCleared; y++) {
                final Color tile = board.tiles.get(x, y);

                if (tile != null) {
                    boardCleared = Boolean.FALSE;
                }
            }
        }

        board.messageDispatcher.dispatchMessage(GameConstants.EVENT_GAMEOVER, boardCleared);
    }
}
