package com.razzeeyy.classicsamematch.models.states;

import com.badlogic.gdx.utils.Array;
import com.razzeeyy.classicsamematch.GameConstants;
import com.razzeeyy.classicsamematch.models.Board;
import com.razzeeyy.classicsamematch.models.Move;

/**
 * Reads the Move history and undoes if there is anything worth undoing
 * Created by razzeeyy on 21.01.17.
 */

public class UndoBoardState extends AbstractBoardState {
    @Override
    public void update(Board board) {
        Array<Move> moveHistory = board.moveHistory;

        if (moveHistory.size > 0) {
            board.messageDispatcher.dispatchMessage(GameConstants.EVENT_UNDO);

            while (moveHistory.size > 0) {
                Move currentMove = moveHistory.pop();
                currentMove.reverse();

                switch (currentMove.getId()) {
                    case Move.ADD:
                        board.addTile(currentMove.getX(), currentMove.getY(), currentMove.getColor());
                        break;
                    case Move.MOVE:
                        board.moveTile(currentMove.getX(), currentMove.getY(), currentMove.getToX(), currentMove.getToY());
                        break;
                }

                board.movePool.free(currentMove);

                if (currentMove.isMatch()) {
                    break;
                }
            }
        }

        board.stateMachine.changeState(board.STATE_IDLE);
    }
}
