package com.razzeeyy.classicsamematch.models.states;

import com.badlogic.gdx.graphics.Color;
import com.razzeeyy.classicsamematch.GameConstants;
import com.razzeeyy.classicsamematch.models.Board;
import com.razzeeyy.classicsamematch.utils.Array2;

/**
 * This is state of the board responsible for making "floating" tiles "fall".
 * Created by razzeeyy on 21.01.17.
 */

public class FallBoardState extends AbstractBoardState {
    @Override
    public void update(Board board) {
        final Array2<Color> tiles = board.tiles;
        boolean shouldEmit = true;

        for (int x = 0; x < tiles.width; x++) {
            for (int y = 0; y < tiles.height; y++) {
                if (tiles.get(x, y) == null) {
                    for (int fromY = y + 1; fromY < tiles.height; fromY++) {
                        if (tiles.get(x, fromY) != null) {
                            if (shouldEmit) { //emit general fall event if any tiles is about to fall
                                board.messageDispatcher.dispatchMessage(GameConstants.EVENT_FALL);
                                shouldEmit = false;
                            }

                            board.moveTile(x, fromY, x, y);
                            break;
                        }
                    }
                }
            }
        }

        board.stateMachine.changeState(board.STATE_SLIDE);
    }
}
