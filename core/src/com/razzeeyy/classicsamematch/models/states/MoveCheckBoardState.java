package com.razzeeyy.classicsamematch.models.states;

import com.badlogic.gdx.graphics.Color;
import com.razzeeyy.classicsamematch.models.Board;
import com.razzeeyy.classicsamematch.utils.Array2;

/**
 * Check if there are any possible moves left on a board model
 * Created by razzeeyy on 21.01.17.
 */

public class MoveCheckBoardState extends AbstractBoardState {
    @Override
    public void update(Board board) {
        final Array2<Color> tiles = board.tiles;

        boolean hasMoves = false;

        for (int x = 0; x < tiles.width; x++) {
            for (int y = 0; y < tiles.height; y++) {
                final Color tile = tiles.get(x, y);

                if (tile != null) {
                    final Color rightTile = tiles.contains(x + 1, y) ? tiles.get(x + 1, y) : null;

                    if (rightTile != null && rightTile == tile) {
                        hasMoves = true;
                        break;
                    }

                    final Color upTile = tiles.contains(x, y + 1) ? tiles.get(x, y + 1) : null;

                    if (upTile != null && upTile == tile) {
                        hasMoves = true;
                        break;
                    }
                }
            }
        }

        if (hasMoves) {
            board.stateMachine.changeState(board.STATE_IDLE);
        } else {
            board.stateMachine.changeState(board.STATE_GAMEOVER);
        }
    }
}
