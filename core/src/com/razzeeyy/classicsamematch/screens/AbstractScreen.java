package com.razzeeyy.classicsamematch.screens;

import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.ai.msg.MessageDispatcher;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.razzeeyy.classicsamematch.ClassicSameMatchGame;

/**
 * Base screen for all screens in game.
 * Screens can be thought of as a global application states (menu, game, endgame screen, etc.).
 * Created by razze on 16.01.2017.
 */

abstract class AbstractScreen extends ScreenAdapter {
    final ClassicSameMatchGame game;

    final Color backgroundColor;

    final Stage gameStage;
    final Stage uiStage;

    final AssetManager assetManager;

    final MessageDispatcher messageDispatcher;

    AbstractScreen(ClassicSameMatchGame game) {
        this.game = game;

        this.backgroundColor = game.backgroundColor;

        this.gameStage = game.gameStage;
        this.uiStage = game.uiStage;

        this.assetManager = game.assetManager;

        this.messageDispatcher = game.messageDispatcher;
    }
}
