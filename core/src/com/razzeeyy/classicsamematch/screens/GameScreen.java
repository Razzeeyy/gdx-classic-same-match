package com.razzeeyy.classicsamematch.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.ai.msg.Telegraph;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.razzeeyy.classicsamematch.ClassicSameMatchGame;
import com.razzeeyy.classicsamematch.GameConstants;
import com.razzeeyy.classicsamematch.actors.ScoreDisplayActor;
import com.razzeeyy.classicsamematch.actors.UndoButton;
import com.razzeeyy.classicsamematch.models.Board;
import com.razzeeyy.classicsamematch.views.BoardView;

/**
 * Well the actual main gameplay screen
 * Created by razze on 18.01.2017.
 */

class GameScreen extends AbstractScreen implements Telegraph {
    private Board board;
    private BoardView boardView;

    private UndoButton undoButton;
    private ScoreDisplayActor scoreDisplayActor;

    GameScreen(ClassicSameMatchGame game) {
        super(game);
    }

    @Override
    public void show() {
        backgroundColor.set(Color.WHITE);

        messageDispatcher.addListeners(this, GameConstants.EVENT_VIEW_GAMEOVER, GameConstants.EVENT_MATCH);

        board = new Board(messageDispatcher, GameConstants.BOARD_WIDTH, GameConstants.BOARD_HEIGHT, GameConstants.colors);
        gameStage.addActor(board);

        boardView = new BoardView(board, assetManager.get(GameConstants.ASSET_TILE, Texture.class),
                assetManager.get(GameConstants.ASSET_MATCH_SOUND, Sound.class), //success match
                assetManager.get(GameConstants.ASSET_MATCH_FAIL_SOUND, Sound.class), //bad match
                assetManager.get(GameConstants.ASSET_FALL_SOUND, Sound.class), //fall
                assetManager.get(GameConstants.ASSET_SLIDE_SOUND, Sound.class), //slide
                assetManager.get(GameConstants.ASSET_UNDO_SOUND, Sound.class), //undo
                assetManager.get(GameConstants.ASSET_PARTICLES, ParticleEffect.class));
        gameStage.addActor(boardView);

        boardView.hookListeners(messageDispatcher);

        board.populate();

        undoButton = new UndoButton(assetManager.get(GameConstants.ASSET_UNDO, Texture.class),
                assetManager.get(GameConstants.ASSET_BUTTON_SOUND, Sound.class));
        undoButton.setTouchable(Touchable.disabled); //undo button gets enabled on match event
        uiStage.addActor(undoButton);

        undoButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                undoButton.setTouchable(Touchable.disabled);
                final int height = Gdx.graphics.getHeight();
                undoButton.addAction(Actions.moveTo(-undoButton.getWidth(), height-undoButton.getHeight()*1.1f, 0.2f));

                board.undo();
            }
        });

        scoreDisplayActor = new ScoreDisplayActor(assetManager.get(GameConstants.ASSET_FONT, BitmapFont.class));
        scoreDisplayActor.setOrigin(scoreDisplayActor.getWidth()/2, scoreDisplayActor.getHeight()/2);
        uiStage.addActor(scoreDisplayActor);

        scoreDisplayActor.hookListeners(messageDispatcher);
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        final float width = Gdx.graphics.getWidth();
        final float height = Gdx.graphics.getHeight();
        scoreDisplayActor.setPosition(width-scoreDisplayActor.getPrefWidth()*1.1f, height-scoreDisplayActor.getPrefHeight()*1.2f);
    }

    @Override
    public void resize(int width, int height) {
        final float undoButtonSize = Math.min(width, height)/6;
        undoButton.setSize(undoButtonSize, undoButtonSize);
        undoButton.setOrigin(undoButton.getWidth()/2, undoButton.getHeight()/2);
        if(undoButton.getTouchable() == Touchable.enabled) {
            undoButton.setPosition(undoButton.getWidth()*0.1f, height-undoButton.getHeight()*1.1f);
        }
        else {
            undoButton.setPosition(-undoButton.getWidth(), height-undoButton.getHeight()*1.1f);
        }
    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void dispose() {
        board.remove();
        board.clearMoveHistory();

        boardView.remove();
        boardView.clear();
        boardView.unhookListeners(messageDispatcher);

        undoButton.remove();
        undoButton.clear();

        messageDispatcher.removeListener(this, GameConstants.EVENT_VIEW_GAMEOVER, GameConstants.EVENT_MATCH);

        scoreDisplayActor.remove();
        scoreDisplayActor.clear();
        scoreDisplayActor.unhookListeners(messageDispatcher);
    }

    @Override
    public boolean handleMessage(Telegram msg) {
        switch(msg.message) {
            case GameConstants.EVENT_VIEW_GAMEOVER:
                switchToGameOver((Boolean) msg.extraInfo);
                return true;
            case GameConstants.EVENT_MATCH:
                final int height = Gdx.graphics.getHeight();
                undoButton.setTouchable(Touchable.enabled);
                undoButton.addAction(Actions.moveTo(undoButton.getWidth()*0.1f, height-undoButton.getHeight()*1.1f, 0.5f));
                return true;
            default:
                return false;
        }
    }

    private void switchToGameOver(Boolean boardCleared) {
        game.setScreen(new GameOverScreen(game, board, scoreDisplayActor.getScore(), boardCleared));
    }
}
