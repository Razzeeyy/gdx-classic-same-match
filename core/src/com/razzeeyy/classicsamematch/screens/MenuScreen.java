package com.razzeeyy.classicsamematch.screens;

import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.ai.msg.Telegraph;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.razzeeyy.classicsamematch.ClassicSameMatchGame;
import com.razzeeyy.classicsamematch.GameConstants;
import com.razzeeyy.classicsamematch.actors.PlayButton;
import com.razzeeyy.classicsamematch.models.Board;
import com.razzeeyy.classicsamematch.views.BoardView;

import java.util.Random;

/**
 * Well... Displays a menu, right?
 * Also features self playing board in the background
 * Created by razze on 18.01.2017.
 */

class MenuScreen extends AbstractScreen implements Telegraph {
    private PlayButton playButton;

    private Board board;
    private BoardView boardView;

    private final Random random = new Random();

    public MenuScreen(ClassicSameMatchGame game) {
        super(game);
    }

    @Override
    public void show() {
        backgroundColor.set(Color.WHITE);

        playButton = new PlayButton(assetManager.get(GameConstants.ASSET_PLAY, Texture.class),
                assetManager.get(GameConstants.ASSET_BUTTON_SOUND, Sound.class));
        uiStage.addActor(playButton);

        playButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                switchToGame();
            }
        });

        messageDispatcher.addListener(this, GameConstants.EVENT_VIEW_GAMEOVER);

        board = new Board(messageDispatcher, GameConstants.BOARD_WIDTH, GameConstants.BOARD_HEIGHT, GameConstants.colors);
        gameStage.addActor(board);

        boardView = new BoardView(board, assetManager.get(GameConstants.ASSET_TILE, Texture.class), null, null, null, null, null,
                assetManager.get(GameConstants.ASSET_PARTICLES, ParticleEffect.class));
        boardView.setTouchable(Touchable.disabled);
        gameStage.addActor(boardView);

        boardView.hookListeners(messageDispatcher);

        board.populate();
    }

    @Override
    public void render(float delta) {
        if(board.stateMachine.getCurrentState() == board.STATE_IDLE && boardView.getActions().size == 0) {
            board.match(random.nextInt(board.tiles.width), random.nextInt(board.tiles.height));
        }
    }

    @Override
    public void resize(int width, int height) {
        final float playButtonSize = Math.min(width, height)/3;
        playButton.setSize(playButtonSize, playButtonSize);
        playButton.setOrigin(playButton.getWidth() / 2, playButton.getHeight() / 2);
        playButton.setPosition((width - playButton.getWidth()) / 2, (height - playButton.getHeight()) / 2);
    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void dispose() {
        playButton.remove();

        board.remove();
        board.clearMoveHistory();

        boardView.remove();
        boardView.clear();
        boardView.unhookListeners(messageDispatcher);

        messageDispatcher.removeListener(this, GameConstants.EVENT_VIEW_GAMEOVER);
    }

    private void switchToGame() {
        game.setScreen(new GameScreen(game));
    }

    @Override
    public boolean handleMessage(Telegram msg) {
        switch (msg.message) {
            case GameConstants.EVENT_VIEW_GAMEOVER:
                board.repopulate();
                return true;
            default:
                return false;
        }
    }
}
