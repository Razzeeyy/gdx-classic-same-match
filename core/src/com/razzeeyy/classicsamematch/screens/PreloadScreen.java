package com.razzeeyy.classicsamematch.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.TextureLoader;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.razzeeyy.classicsamematch.ClassicSameMatchGame;
import com.razzeeyy.classicsamematch.GameConstants;
import com.razzeeyy.classicsamematch.actors.SpriteActor;

/**
 * Shows a simple preloading bar while takes care of loading the necessary resources for the game
 * Created by razze on 16.01.2017.
 */

public class PreloadScreen extends AbstractScreen {
    private Pixmap preloadPixmap;
    private Texture preloadTexture;
    private SpriteActor preloadImage;

    public PreloadScreen(ClassicSameMatchGame game) {
        super(game);
    }

    @Override
    public void show() {
        TextureLoader.TextureParameter textureParameter = new TextureLoader.TextureParameter();
        textureParameter.minFilter = Texture.TextureFilter.Linear;
        textureParameter.magFilter = Texture.TextureFilter.Linear;

        assetManager.load(GameConstants.ASSET_PLAY, Texture.class, textureParameter);
        assetManager.load(GameConstants.ASSET_PREVIEW, Texture.class, textureParameter);
        assetManager.load(GameConstants.ASSET_REFRESH, Texture.class, textureParameter);
        assetManager.load(GameConstants.ASSET_TILE, Texture.class, textureParameter);
        assetManager.load(GameConstants.ASSET_UNDO, Texture.class, textureParameter);

        assetManager.load(GameConstants.ASSET_BUTTON_SOUND, Sound.class);
        assetManager.load(GameConstants.ASSET_MATCH_SOUND, Sound.class);
        assetManager.load(GameConstants.ASSET_REPLAY_SOUND, Sound.class);
        assetManager.load(GameConstants.ASSET_MATCH_FAIL_SOUND, Sound.class);
        assetManager.load(GameConstants.ASSET_FALL_SOUND, Sound.class);
        assetManager.load(GameConstants.ASSET_SLIDE_SOUND, Sound.class);
        assetManager.load(GameConstants.ASSET_UNDO_SOUND, Sound.class);

        assetManager.load(GameConstants.ASSET_FONT, BitmapFont.class);

        assetManager.load(GameConstants.ASSET_PARTICLES, ParticleEffect.class);
        assetManager.load(GameConstants.ASSET_PARTICLES_BOARD_CLEAR, ParticleEffect.class);

        backgroundColor.set(Color.BLACK);

        preloadPixmap = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
        preloadPixmap.setColor(Color.WHITE);
        preloadPixmap.fill();

        preloadTexture = new Texture(preloadPixmap);

        preloadImage = new SpriteActor(preloadTexture);
        uiStage.addActor(preloadImage);
    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void dispose() {
        preloadImage.remove();
        preloadImage.clear();
        preloadPixmap.dispose();
        preloadTexture.dispose();
    }

    @Override
    public void render(float delta) {
        if (assetManager.update()) {
            //make fonts use linear filtering
            BitmapFont font = assetManager.get(GameConstants.ASSET_FONT, BitmapFont.class);
            font.getData().setScale(Gdx.graphics.getDensity()*1.25f);
            for (TextureRegion textureRegion : font.getRegions()) {
                textureRegion.getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            }

            //and start the game
            switchToMenu();
        }

        final int width = Gdx.graphics.getWidth();
        final int height = Gdx.graphics.getHeight();

        preloadImage.setSize(width * assetManager.getProgress(), height / 6);
        preloadImage.setOrigin(preloadImage.getWidth() / 2, preloadImage.getHeight() / 2);
        preloadImage.setPosition(0, (height - preloadImage.getHeight()) / 2);
    }

    private void switchToMenu() {
        game.setScreen(new MenuScreen(game));
    }
}
