package com.razzeeyy.classicsamematch.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.razzeeyy.classicsamematch.ClassicSameMatchGame;
import com.razzeeyy.classicsamematch.GameConstants;
import com.razzeeyy.classicsamematch.actors.ReplayButton;
import com.razzeeyy.classicsamematch.actors.ScoreDisplayActor;
import com.razzeeyy.classicsamematch.models.Board;
import com.razzeeyy.classicsamematch.views.StaticDisplayBoardView;

/**
 * Shows record score & current personal score & replay button
 * Current score is always highlighted green and is flash for a few times when shown
 * Created by razze on 18.01.2017.
 */

class GameOverScreen extends AbstractScreen {
    private final StaticDisplayBoardView displayBoardView;
    private final ScoreDisplayActor higherScoreDisplayActor;
    private final ScoreDisplayActor lowerScoreDisplayActor;
    private final ScoreDisplayActor currentScore;
    private Group scoreDisplayGroup;
    private ReplayButton replayButton;
    private ParticleEffect boardClearParticles;
    private Actor particles;

    GameOverScreen(ClassicSameMatchGame game, Board board, int score, boolean boardCleared) {
        super(game);
        final String SAVE_HIGHSCORE = "highscore";

        displayBoardView = new StaticDisplayBoardView(board, assetManager.get(GameConstants.ASSET_TILE, Texture.class));

        Preferences saves = Gdx.app.getPreferences(ClassicSameMatchGame.class.getName());
        final int oldHigh = saves.getInteger(SAVE_HIGHSCORE, 0);

        higherScoreDisplayActor = new ScoreDisplayActor(assetManager.get(GameConstants.ASSET_FONT, BitmapFont.class), Color.WHITE);
        lowerScoreDisplayActor = new ScoreDisplayActor(assetManager.get(GameConstants.ASSET_FONT, BitmapFont.class), Color.WHITE);

        if (score > oldHigh) {
            saves.putInteger(SAVE_HIGHSCORE, score);
            saves.flush();

            higherScoreDisplayActor.setScore(score);
            higherScoreDisplayActor.setColor(Color.GREEN);
            currentScore = higherScoreDisplayActor;

            lowerScoreDisplayActor.setScore(oldHigh);
            lowerScoreDisplayActor.setColor(Color.BLACK);
        } else {
            higherScoreDisplayActor.setScore(oldHigh);
            higherScoreDisplayActor.setColor(Color.BLACK);

            lowerScoreDisplayActor.setScore(score);
            lowerScoreDisplayActor.setColor(Color.GREEN);
            currentScore = lowerScoreDisplayActor;
        }

        if (higherScoreDisplayActor.getScore() == 0) {
            higherScoreDisplayActor.setVisible(false);
        }
        if (lowerScoreDisplayActor.getScore() == 0) {
            lowerScoreDisplayActor.setVisible(false);
        }

        if (boardCleared) {
            boardClearParticles = assetManager.get(GameConstants.ASSET_PARTICLES_BOARD_CLEAR, ParticleEffect.class);
            boardClearParticles.setPosition(gameStage.getWidth() / 2, gameStage.getHeight() / 2);
        }
    }

    @Override
    public void show() {
        backgroundColor.set(Color.WHITE);

        displayBoardView.addAction(Actions.alpha(0.5f, 0.5f));
        gameStage.addActor(displayBoardView);

        scoreDisplayGroup = new Group();
        uiStage.addActor(scoreDisplayGroup);

        scoreDisplayGroup.addActor(higherScoreDisplayActor);
        scoreDisplayGroup.addActor(lowerScoreDisplayActor);

        higherScoreDisplayActor.getColor().a = 0;
        higherScoreDisplayActor.addAction(Actions.fadeIn(1f));
        lowerScoreDisplayActor.getColor().a = 0;
        lowerScoreDisplayActor.addAction(Actions.fadeIn(1f));

        currentScore.clearActions();
        currentScore.addAction(Actions.repeat(3, Actions.sequence(Actions.fadeOut(0.3f), Actions.fadeIn(0.3f))));

        higherScoreDisplayActor.setOrigin(higherScoreDisplayActor.getPrefWidth() / 2, higherScoreDisplayActor.getPrefHeight() / 2);
        lowerScoreDisplayActor.setOrigin(lowerScoreDisplayActor.getPrefWidth() / 2, lowerScoreDisplayActor.getPrefHeight() / 2);

        lowerScoreDisplayActor.setPosition(
                (higherScoreDisplayActor.getPrefWidth() - lowerScoreDisplayActor.getPrefWidth()) / 2,
                -higherScoreDisplayActor.getPrefHeight() * 1.7f);

        replayButton = new ReplayButton(assetManager.get(GameConstants.ASSET_REFRESH, Texture.class),
                assetManager.get(GameConstants.ASSET_REPLAY_SOUND, Sound.class));
        uiStage.addActor(replayButton);

        replayButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                switchToGame();
            }
        });

        if (boardClearParticles != null) {
            boardClearParticles.start();

            particles = new Actor() {
                @Override
                public void draw(Batch batch, float parentAlpha) {
                    if (!boardClearParticles.isComplete()) {
                        boardClearParticles.draw(batch, Gdx.graphics.getDeltaTime());
                    }
                }
            };
            gameStage.addActor(particles);
        }
    }

    @Override
    public void resize(int width, int height) {
        final float replayButtonSize = Math.min(width, height) / 8;
        final float scale = replayButtonSize / Math.min(replayButton.getWidth(), replayButton.getHeight());
        replayButton.setSize(replayButton.getWidth() * scale, replayButton.getHeight() * scale);
        replayButton.setOrigin(replayButton.getWidth() / 2, replayButton.getHeight() / 2);
        replayButton.setPosition((width - replayButton.getWidth()) / 2, height * 0.33f);

        final float groupWidth = higherScoreDisplayActor.getPrefWidth() != 0 ? higherScoreDisplayActor.getPrefWidth() : lowerScoreDisplayActor.getPrefWidth();
        scoreDisplayGroup.setPosition((width - groupWidth) * 0.5f, height * 0.75f);
    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void dispose() {
        displayBoardView.remove();
        displayBoardView.clear();

        scoreDisplayGroup.remove();
        scoreDisplayGroup.clear();

        higherScoreDisplayActor.remove();
        higherScoreDisplayActor.clear();

        lowerScoreDisplayActor.remove();
        lowerScoreDisplayActor.clear();

        replayButton.remove();

        if (particles != null) particles.remove();
    }

    private void switchToGame() {
        game.setScreen(new GameScreen(game));
    }
}
