package com.razzeeyy.classicsamematch;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.ai.GdxAI;
import com.badlogic.gdx.ai.msg.MessageDispatcher;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.razzeeyy.classicsamematch.screens.PreloadScreen;

public class ClassicSameMatchGame extends Game {
    private int GL_COVERAGE_BUFFER_BIT = 0;

    public Color backgroundColor;

    public Stage gameStage;
    public Stage uiStage;

    public AssetManager assetManager;

    public MessageDispatcher messageDispatcher;

    @Override
    public void create() {
        GL_COVERAGE_BUFFER_BIT = Gdx.graphics.getBufferFormat().coverageSampling ? GL20.GL_COVERAGE_BUFFER_BIT_NV : 0;

        backgroundColor = Color.BLACK.cpy();

        gameStage = new Stage(new FitViewport(GameConstants.BOARD_WIDTH, GameConstants.BOARD_HEIGHT));
        uiStage = new Stage(new ScreenViewport());

        Gdx.input.setInputProcessor(new InputMultiplexer(uiStage, gameStage));

        assetManager = new AssetManager();

        messageDispatcher = new MessageDispatcher();

        setScreen(new PreloadScreen(this));
    }

    @Override
    public void dispose() {
        super.dispose();

        backgroundColor.set(Color.RED);

        gameStage.dispose();
        uiStage.dispose();

        assetManager.dispose();

        messageDispatcher.clear();
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(backgroundColor.r, backgroundColor.g, backgroundColor.b, backgroundColor.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT | GL_COVERAGE_BUFFER_BIT);

        GdxAI.getTimepiece().update(Gdx.graphics.getDeltaTime());
        messageDispatcher.update();

        gameStage.getViewport().apply();
        gameStage.act();
        gameStage.draw();

        uiStage.getViewport().apply();
        uiStage.act();
        uiStage.draw();

        super.render();
    }

    @Override
    public void resize(int width, int height) {
        uiStage.getViewport().update(width, height, true);
        gameStage.getViewport().update(width, height, true);

        super.resize(width, height);
    }
}
