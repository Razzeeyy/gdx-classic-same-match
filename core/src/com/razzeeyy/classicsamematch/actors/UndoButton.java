package com.razzeeyy.classicsamematch.actors;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;

/**
 * Asks for an undo... Politely
 * Created by razze on 18.01.2017.
 */

public class UndoButton extends ButtonActor {
    public UndoButton(Texture texture, Sound sound) {
        super(texture, sound);
    }
}
