package com.razzeeyy.classicsamematch.actors;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

/**
 * Lets play this game again over and over!
 * Created by razze on 18.01.2017.
 */

public class ReplayButton extends ButtonActor {
    private final float ON_HOVER_SCALE_TIME = 0.3f;
    private final float ON_HOVER_SCALE_VALUE = 1.25f;
    private final float ROTATE_DEGREE = 360*3;
    private final float ROTATE_TIME = 1;

    private final Runnable changeEventRunnable = new Runnable() {
        @Override
        public void run() {
            emitChangeEvent();
        }
    };

    public ReplayButton(Texture texture, Sound sound) {
        super(texture, sound);

        removeListener(buttonInputHandler);

        addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                buttonSound.play();

                addAction(Actions.sequence(
                        Actions.rotateTo(ROTATE_DEGREE, ROTATE_TIME),
                        Actions.run(changeEventRunnable)
                ));

                return true;
            }

            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                addAction(Actions.scaleTo(ON_HOVER_SCALE_VALUE, ON_HOVER_SCALE_VALUE, ON_HOVER_SCALE_TIME));
            }

            @Override
            public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
                addAction(Actions.scaleTo(1, 1, ON_HOVER_SCALE_TIME));
            }
        });
    }
}
