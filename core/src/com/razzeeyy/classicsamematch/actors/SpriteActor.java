package com.razzeeyy.classicsamematch.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Displays a sprite. Was used instead of ImageActor from scene2d.ui because scene2d.ui widgets don't seem to handle scaling properly
 * And I thought this sprite based actor would perform somewhat faster or something like that
 * Created by razze on 18.01.2017.
 */

public class SpriteActor extends Actor {
    private final Sprite sprite;

    public SpriteActor(Texture texture) {
        //can't use scene2d.ui Image as base because it doesn't support proper scaling
        sprite = new Sprite(texture);
        setSize(sprite.getWidth(), sprite.getHeight());
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        final Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        batch.draw(sprite, getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
    }
}
