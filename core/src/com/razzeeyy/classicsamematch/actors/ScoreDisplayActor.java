package com.razzeeyy.classicsamematch.actors;

import com.badlogic.gdx.ai.msg.MessageDispatcher;
import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.ai.msg.Telegraph;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.razzeeyy.classicsamematch.GameConstants;
import com.razzeeyy.classicsamematch.models.Move;

/**
 * Displays score
 * Created by razzeeyy on 05.02.17.
 */

public class ScoreDisplayActor extends Label implements Telegraph {
    private int score = 0;
    private int lastIncrement = 0;

    private final Runnable updateScore = new Runnable() {
        @Override
        public void run() {
            setScore(score);
        }
    };

    public ScoreDisplayActor(BitmapFont bitmapFont, Color color) {
        super("0", new LabelStyle(bitmapFont, color));
    }

    public ScoreDisplayActor(BitmapFont bitmapFont) {
        this(bitmapFont, Color.BLACK);
    }

    public void setScore(int score) {
        this.score = score;
        setText(Integer.toString(score));
    }

    public int getScore() {
        return score;
    }

    public void hookListeners(MessageDispatcher messageDispatcher) {
        messageDispatcher.addListeners(this,
                GameConstants.EVENT_MATCH,
                GameConstants.EVENT_UNDO,
                GameConstants.EVENT_GAMEOVER);
    }

    public void unhookListeners(MessageDispatcher messageDispatcher) {
        messageDispatcher.removeListener(this,
                GameConstants.EVENT_MATCH,
                GameConstants.EVENT_UNDO,
                GameConstants.EVENT_GAMEOVER);
    }

    @Override
    public boolean handleMessage(Telegram msg) {
        switch(msg.message) {
            case GameConstants.EVENT_MATCH:
                Move move = (Move) msg.extraInfo;
                if(move.isSuccessfulMatch()) {
                    //scoring formula is: pow(n-1, 2)
                    lastIncrement = move.getMatchLength()-1;
                    lastIncrement *= lastIncrement;
                    score += lastIncrement;

                    addAction(Actions.sequence(Actions.fadeOut(0.2f), Actions.run(updateScore), Actions.fadeIn(0.1f)));
                }
                return true;
            case GameConstants.EVENT_UNDO:
                score -= lastIncrement;
                setScore(score);

                addAction(Actions.sequence(Actions.fadeOut(0.2f), Actions.run(updateScore), Actions.fadeIn(0.1f)));
                return true;
            case GameConstants.EVENT_GAMEOVER:
                Boolean boardCleared = (Boolean) msg.extraInfo;
                if(boardCleared) {
                    score *= 2; //double the score if board is cleared completely
                    setScore(score);

                    addAction(Actions.sequence(Actions.fadeOut(0.2f), Actions.run(updateScore), Actions.fadeIn(0.1f)));
                }
                return  true;
        }

        return false;
    }
}
