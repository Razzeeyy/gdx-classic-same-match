package com.razzeeyy.classicsamematch.actors;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

/**
 * Well Starts the game when pressed...
 * Created by razze on 18.01.2017.
 */

public class PlayButton extends ButtonActor {
    private final float ON_HOVER_SCALE_TIME = 0.3f;
    private final float ON_HOVER_SCALE_VALUE = 1.25f;

    public PlayButton(Texture texture, Sound sound) {
        super(texture, sound);

        addListener(new InputListener() {
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                addAction(Actions.scaleTo(ON_HOVER_SCALE_VALUE, ON_HOVER_SCALE_VALUE, ON_HOVER_SCALE_TIME));
            }

            @Override
            public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
                addAction(Actions.scaleTo(1, 1, ON_HOVER_SCALE_TIME));
            }
        });
    }
}
