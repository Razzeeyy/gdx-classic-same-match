package com.razzeeyy.classicsamematch.actors;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Pools;

/**
 * A base button actor.
 * It was used instead of scene2d.ui button, because scene2d.ui widgets don't handle setScale() method properly
 * Created by razzeeyy on 23.01.17.
 */

class ButtonActor extends SpriteActor {
    final Sound buttonSound;

    final InputListener buttonInputHandler = new InputListener() {
        @Override
        public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            buttonSound.play();
            emitChangeEvent();
            return true;
        }
    };

    ButtonActor(Texture texture, Sound sound) {
        super(texture);

        buttonSound = sound;

        addListener(buttonInputHandler);
    }

    void emitChangeEvent() {
        ChangeListener.ChangeEvent changeEvent = Pools.obtain(ChangeListener.ChangeEvent.class);

        fire(changeEvent);

        Pools.free(changeEvent);
    }
}
