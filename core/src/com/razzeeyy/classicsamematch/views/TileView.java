package com.razzeeyy.classicsamematch.views;

import com.badlogic.gdx.graphics.Texture;
import com.razzeeyy.classicsamematch.actors.SpriteActor;

/**
 * Visually displays a tile in BoardView according to it's model
 * Created by razzeeyy on 18.01.17.
 */

public class TileView extends SpriteActor {

    public TileView(Texture texture) {
        super(texture);
        setSize(1, 1);
    }
}
