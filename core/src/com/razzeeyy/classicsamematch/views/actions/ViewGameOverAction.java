package com.razzeeyy.classicsamematch.views.actions;

import com.badlogic.gdx.ai.msg.MessageDispatcher;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.razzeeyy.classicsamematch.GameConstants;

/**
 * Notified that view completed animating and ready to switch to game end screen
 * Created by razze on 12.02.2017.
 */

public class ViewGameOverAction extends Action {
    private MessageDispatcher messageDispatcher;
    private Boolean boardCleared;

    public void initialize(MessageDispatcher messageDispatcher, Boolean boardCleared) {
        this.messageDispatcher = messageDispatcher;
        this.boardCleared = boardCleared;
    }

    @Override
    public boolean act(float delta) {
        messageDispatcher.dispatchMessage(GameConstants.EVENT_VIEW_GAMEOVER, boardCleared);
        return true;
    }
}
