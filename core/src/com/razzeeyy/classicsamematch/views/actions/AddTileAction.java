package com.razzeeyy.classicsamematch.views.actions;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.razzeeyy.classicsamematch.views.BoardView;
import com.razzeeyy.classicsamematch.views.TileView;

/**
 * This action will generate and add a new TileView to BoardView
 * Created by razze on 03.02.2017.
 */

public class AddTileAction extends Action {
    private BoardView boardView;
    private int x;
    private int y;
    private Color color;

    public void initialize(BoardView boardView, int x, int y, Color color) {
        this.boardView = boardView;
        this.x = x;
        this.y = y;
        this.color = color;
    }

    @Override
    public boolean act(float delta) {
        TileView tileView = new TileView(boardView.tileTexture);
        boardView.tileViews.set(x, y, tileView);

        tileView.setPosition(x, y);
        tileView.setColor(color);
        tileView.addListener(boardView.tileInputListener);
        boardView.addActor(tileView);

        tileView.getColor().a = 0;
        Action tileViewFadeIn = Actions.fadeIn(boardView.random.nextFloat()*0.3f);
        tileViewFadeIn.setTarget(tileView);
        boardView.addAction(tileViewFadeIn);

        return true;
    }
}
