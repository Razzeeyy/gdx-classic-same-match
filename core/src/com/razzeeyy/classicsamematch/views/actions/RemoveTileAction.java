package com.razzeeyy.classicsamematch.views.actions;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.razzeeyy.classicsamematch.views.BoardView;
import com.razzeeyy.classicsamematch.views.TileView;

/**
 * Removes a TileView from a BoardView
 * Created by razze on 03.02.2017.
 */

public class RemoveTileAction extends Action {
    private BoardView boardView;
    private int x;
    private int y;

    public void initialize(BoardView boardView, int x, int y) {
        this.boardView = boardView;
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean act(float delta) {
        TileView tileView = boardView.tileViews.get(x, y);
        boardView.tileViews.set(x, y, null);

        Action tileViewFadeOut = Actions.fadeOut(0.1f);
        tileViewFadeOut.setTarget(tileView);
        removeTileView.setTarget(tileView);

        boardView.addAction(Actions.sequence(tileViewFadeOut, removeTileView));

        return true;
    }

    private final Action removeTileView = new Action() {
        @Override
        public boolean act(float delta) {
            target.remove();
            target.clear();
            return true;
        }
    };
}
