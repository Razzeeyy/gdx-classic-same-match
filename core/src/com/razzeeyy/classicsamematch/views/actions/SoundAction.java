package com.razzeeyy.classicsamematch.views.actions;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.scenes.scene2d.Action;

/**
 * Lets play some sweet sounds.
 * Created by razze on 12.02.2017.
 */

public class SoundAction extends Action {
    private Sound sound;

    public void initialize(Sound sound) {
        this.sound = sound;
    }

    @Override
    public boolean act(float delta) {
        if(sound != null) sound.play();
        return true;
    }
}
