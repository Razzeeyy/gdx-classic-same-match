package com.razzeeyy.classicsamematch.views.actions;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.utils.Array;

/**
 * Spawns a particles at the specified position with specified color.
 * Created by razze on 03.02.2017.
 */

public class ParticlesAction extends Action {
    private Array<ParticleEffect> particles;
    private ParticleEffect particleEffect;
    private int x;
    private int y;
    private Color color;

    public void initialize(Array<ParticleEffect> particles, ParticleEffect particleEffect, int x, int y, Color color) {
        this.particles = particles;
        this.particleEffect = particleEffect;
        this.x = x;
        this.y = y;
        this.color = color;
    }

    @Override
    public boolean act(float delta) {
        particleEffect.setPosition(x+0.5f, y+0.5f);

        if(color != null) {
            for (ParticleEmitter particleEmitter : particleEffect.getEmitters()) {
                float[] colors = particleEmitter.getTint().getColors();
                for(int i=0; i<colors.length; i+=3) {
                    colors[i] = color.r;
                    colors[i+1] = color.g;
                    colors[i+2] = color.b;
                }
            }
        }

        particles.add(particleEffect);
        particleEffect.start();

        return true;
    }
}
