package com.razzeeyy.classicsamematch.views.actions;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.razzeeyy.classicsamematch.views.BoardView;
import com.razzeeyy.classicsamematch.views.TileView;

/**
 *  Moves/Slides a TileView in BoardView
 * Created by razze on 03.02.2017.
 */

public class SlideTileAction extends Action {
    private BoardView boardView;
    private int x, y, toX;

    public void initialize(BoardView boardView, int x, int y, int toX) {
        this.boardView = boardView;
        this.x = x;
        this.y = y;
        this.toX = toX;
    }

    @Override
    public boolean act(float delta) {
        TileView tileView = boardView.tileViews.get(x, y);
        boardView.tileViews.set(x, y, null);
        boardView.tileViews.set(toX, y, tileView);

        Action tileViewMoveTo = Actions.moveTo(toX, y, 0.4f, Interpolation.bounceOut);
        tileViewMoveTo.setTarget(tileView);
        boardView.addAction(tileViewMoveTo);

        return true;
    }
}
