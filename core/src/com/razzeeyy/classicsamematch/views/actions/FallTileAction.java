package com.razzeeyy.classicsamematch.views.actions;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.razzeeyy.classicsamematch.views.BoardView;
import com.razzeeyy.classicsamematch.views.TileView;

/**
 * Managed movement of the falling tile on the screen in BoardView.
 * Created by razze on 03.02.2017.
 */

public class FallTileAction extends Action {
    private BoardView boardView;
    private int x, y, toY;

    public void initialize(BoardView boardView, int x, int y, int toY) {
        this.boardView = boardView;
        this.x = x;
        this.y = y;
        this.toY = toY;
    }

    @Override
    public boolean act(float delta) {
        TileView tileView = boardView.tileViews.get(x, y);
        boardView.tileViews.set(x, y, null);
        boardView.tileViews.set(x, toY, tileView);

        Action tileViewMoveTo = Actions.moveTo(x, toY, 0.1f, Interpolation.pow2Out);
        tileViewMoveTo.setTarget(tileView);
        boardView.addAction(tileViewMoveTo);

        return true;
    }
}
