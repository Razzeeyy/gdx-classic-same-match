package com.razzeeyy.classicsamematch.views;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.razzeeyy.classicsamematch.models.Board;

/**
 * Used to display a "ghost" of the board on the game end screen
 * Created by razzeeyy on 18.01.17.
 */

public class StaticDisplayBoardView extends Group {
    private final Texture tileTexture;
    private final Board board;

    public StaticDisplayBoardView(Board board, Texture tileTexture) {
        this.board = board;
        this.tileTexture = tileTexture;

        refresh();
    }

    private void refresh() {
        for(int x=0; x<board.tiles.width; x++) {
            for(int y=0; y<board.tiles.height; y++) {
                Color tile = board.tiles.get(x, y);
                if(tile != null) {
                    TileView tileView = new TileView(tileTexture);
                    tileView.setPosition(x, y);
                    tileView.setColor(tile);
                    addActor(tileView);
                }
            }
        }
    }
}
