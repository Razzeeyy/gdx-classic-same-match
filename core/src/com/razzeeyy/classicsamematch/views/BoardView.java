package com.razzeeyy.classicsamematch.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ai.msg.MessageDispatcher;
import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.ai.msg.Telegraph;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Queue;
import com.razzeeyy.classicsamematch.GameConstants;
import com.razzeeyy.classicsamematch.models.Board;
import com.razzeeyy.classicsamematch.models.Move;
import com.razzeeyy.classicsamematch.utils.Array2;
import com.razzeeyy.classicsamematch.views.actions.AddTileAction;
import com.razzeeyy.classicsamematch.views.actions.FallTileAction;
import com.razzeeyy.classicsamematch.views.actions.ParticlesAction;
import com.razzeeyy.classicsamematch.views.actions.SlideTileAction;
import com.razzeeyy.classicsamematch.views.actions.RemoveTileAction;
import com.razzeeyy.classicsamematch.views.actions.SoundAction;
import com.razzeeyy.classicsamematch.views.actions.ViewGameOverAction;

import java.util.Random;

/**
 * BoardView is meant to be a visual part of the board.
 * It displays the TileView accordingly to events that is coming from Board model.
 * Created by razzeeyy on 18.01.17.
 */

public class BoardView extends Group implements Telegraph {
    public final Texture tileTexture;
    public final Array2<TileView> tileViews;
    public final Random random = new Random();

    private final Board board;

    private final Queue<Action> actionsToExecute = new Queue<Action>();

    private final Sound matchSuccessSound;
    private final Sound matchFailSound;
    private final Sound fallSound;
    private final Sound slideSound;
    private final Sound undoSound;
    private final ParticleEffectPool particleEffectPool;

    private final Array<ParticleEffect> particles;

    private MessageDispatcher messageDispatcher;

    public final InputListener tileInputListener = new InputListener() {
        @Override
        public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            final TileView tileView = (TileView) event.getListenerActor();
            board.match((int) tileView.getX(), (int) tileView.getY());
            return true;
        }
    };

    public BoardView(Board board, Texture tileTexture, Sound matchSuccessSound, Sound matchFailSound, Sound fallSound, Sound slideSound, Sound undoSound, ParticleEffect particleEffect) {
        this.board = board;
        this.tileTexture = tileTexture;
        tileViews = new Array2<TileView>(board.tiles.width, board.tiles.height);
        this.matchSuccessSound = matchSuccessSound;
        this.matchFailSound = matchFailSound;
        this.fallSound = fallSound;
        this.slideSound = slideSound;
        this.undoSound = undoSound;
        particleEffectPool = new ParticleEffectPool(particleEffect, 10, board.tiles.width*board.tiles.height);

        particles = new Array<ParticleEffect>();
    }

    public void hookListeners(MessageDispatcher messageDispatcher) {
        messageDispatcher.addListeners(this,
                GameConstants.EVENT_ADD,
                GameConstants.EVENT_REMOVE,
                GameConstants.EVENT_MOVE,
                GameConstants.EVENT_MATCH,
                GameConstants.EVENT_FALL,
                GameConstants.EVENT_SLIDE,
                GameConstants.EVENT_UNDO,
                GameConstants.EVENT_GAMEOVER);

        this.messageDispatcher = messageDispatcher;
    }

    public void unhookListeners(MessageDispatcher messageDispatcher) {
        messageDispatcher.removeListener(this,
                GameConstants.EVENT_ADD,
                GameConstants.EVENT_REMOVE,
                GameConstants.EVENT_MOVE,
                GameConstants.EVENT_MATCH,
                GameConstants.EVENT_FALL,
                GameConstants.EVENT_SLIDE,
                GameConstants.EVENT_UNDO,
                GameConstants.EVENT_GAMEOVER);
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if (actionsToExecute.size > 0 && getActions().size == 0) {
            Action previousAction = null;
            while (actionsToExecute.size > 0) {
                final Action currentAction = actionsToExecute.first();
                if (previousAction == null || currentAction.getClass().equals(previousAction.getClass())
                        || currentAction instanceof ParticlesAction) { //particles actions shouldn't make other action executions wait
                    addAction(currentAction);
                    actionsToExecute.removeFirst();
                    if(!(currentAction instanceof ParticlesAction)) { //particles actions shouldn't interrupt other action execution line at all
                        previousAction = currentAction;
                    }
                } else {
                    break;
                }
            }
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);

        //draw the particles
        for (int i = particles.size - 1; i >= 0; i--) {
            ParticleEffectPool.PooledEffect effect = (ParticleEffectPool.PooledEffect)particles.get(i);
            effect.draw(batch, Gdx.graphics.getDeltaTime());
            if (effect.isComplete()) {
                effect.free();
                particles.removeIndex(i);
            }
        }
    }

    @Override
    public boolean handleMessage(Telegram msg) {
        Move move = null;
        if(msg.message != GameConstants.EVENT_GAMEOVER) {
            move = (Move) msg.extraInfo;
        }

        switch (msg.message) {
            case GameConstants.EVENT_GAMEOVER:
                ViewGameOverAction viewGameOverAction = Actions.action(ViewGameOverAction.class);
                viewGameOverAction.initialize(messageDispatcher, (Boolean) msg.extraInfo);
                actionsToExecute.addLast(viewGameOverAction);
                return true;

            case GameConstants.EVENT_MATCH:
                SoundAction matchSoundAction = Actions.action(SoundAction.class);
                if(move.isSuccessfulMatch()) {
                    matchSoundAction.initialize(matchSuccessSound);
                } else {
                    matchSoundAction.initialize(matchFailSound);
                }
                actionsToExecute.addLast(matchSoundAction);
                return true;

            case GameConstants.EVENT_FALL:
                SoundAction fallSoundAction = Actions.action(SoundAction.class);
                fallSoundAction.initialize(fallSound);
                actionsToExecute.addLast(fallSoundAction);
                return true;

            case GameConstants.EVENT_SLIDE:
                SoundAction slideSoundAction = Actions.action(SoundAction.class);
                slideSoundAction.initialize(slideSound);
                actionsToExecute.addLast(slideSoundAction);
                return true;

            case GameConstants.EVENT_UNDO:
                SoundAction undoSoundAction = Actions.action(SoundAction.class);
                undoSoundAction.initialize(undoSound);
                actionsToExecute.addLast(undoSoundAction);
                return true;

            case GameConstants.EVENT_ADD:
                AddTileAction addTileAction = Actions.action(AddTileAction.class);
                addTileAction.initialize(this, move.getX(), move.getY(), move.getColor());
                actionsToExecute.addLast(addTileAction);
                return true;

            case GameConstants.EVENT_REMOVE:
                RemoveTileAction removeTileAction = Actions.action(RemoveTileAction.class);
                removeTileAction.initialize(this, move.getX(), move.getY());
                actionsToExecute.addLast(removeTileAction);

                ParticlesAction particlesAction = Actions.action(ParticlesAction.class);
                particlesAction.initialize(particles, particleEffectPool.obtain(), move.getX(), move.getY(), move.getColor());
                actionsToExecute.addLast(particlesAction);
                return true;

            case GameConstants.EVENT_MOVE:
                if(move.isFall()) {
                    FallTileAction fallTileAction = Actions.action(FallTileAction.class);
                    fallTileAction.initialize(this, move.getX(), move.getY(), move.getToY());
                    actionsToExecute.addLast(fallTileAction);
                } else {
                    SlideTileAction slideTileAction = Actions.action(SlideTileAction.class);
                    slideTileAction.initialize(this, move.getX(), move.getY(), move.getToX());
                    actionsToExecute.addLast(slideTileAction);
                }
                return true;
        }

        return false;
    }
}
