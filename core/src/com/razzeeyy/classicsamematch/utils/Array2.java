package com.razzeeyy.classicsamematch.utils;

import com.badlogic.gdx.utils.Array;

/**
 * A wrapper over Array to make it easier to use for 2 dimensional tile grid
 * Created by razze on 18.01.2017.
 */

public class Array2<T> {
    public final int width;
    public final int height;
    private final Array<T> array;

    public Array2(int width, int height) {
        this.width = width;
        this.height = height;

        array = new Array<T>(width * height);

        for(int i=0; i<width*height; i++) {
            array.insert(i, null);
        }
    }

    public void clear() {
        array.clear();
    }

    public void set(int x, int y, T value) {
        array.set(index(x, y), value);
    }

    public T get(int x, int y) {
        return array.get(index(x, y));
    }

    public boolean contains(int x, int y) {
        return !(x < 0 || x >= width) && !(y < 0 || y >= height);
    }

    private int index(int x, int y) {
        if(!contains(x, y)) throw new IndexOutOfBoundsException("index out of bounds, x: "+x+", y: "+y);
        return y * width + x;
    }
}
