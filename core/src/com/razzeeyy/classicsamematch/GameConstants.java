package com.razzeeyy.classicsamematch;

import com.badlogic.gdx.graphics.Color;

/**
 * A class with all constants related to the game, serves as sorta global config file. For easy tuning.
 * Created by razze on 16.01.2017.
 */

public abstract class GameConstants {
    public static final int BOARD_WIDTH = 5;
    public static final int BOARD_HEIGHT = 7;

    public static final Color[] colors = {
            new Color(0x990000ff),
            new Color(0x006600ff),
            new Color(0x191970ff),
            new Color(0xCC9900ff),
    };

    public static final String ASSET_PLAY = "play.png";
    public static final String ASSET_PREVIEW = "preview.png";
    public static final String ASSET_REFRESH = "refresh.png";
    public static final String ASSET_TILE = "tile.png";
    public static final String ASSET_UNDO = "undo.png";

    public static final String ASSET_BUTTON_SOUND = "button.wav";
    public static final String ASSET_MATCH_SOUND = "match.wav";
    public static final String ASSET_REPLAY_SOUND = "replay.wav";
    public static final String ASSET_MATCH_FAIL_SOUND = "match_fail.wav";
    public static final String ASSET_FALL_SOUND = "fall.wav";
    public static final String ASSET_SLIDE_SOUND = "slide.wav";
    public static final String ASSET_UNDO_SOUND = "undo.wav";

    public static final String ASSET_FONT = "exo.fnt";

    public static final String ASSET_PARTICLES = "particles.p";
    public static final String ASSET_PARTICLES_BOARD_CLEAR = "salute.p";

    public static final int EVENT_ADD = 1;
    public static final int EVENT_REMOVE = 2;
    public static final int EVENT_MOVE = 3;
    public static final int EVENT_FALL = 4;
    public static final int EVENT_SLIDE = 5;
    public static final int EVENT_MATCH = 6;
    public static final int EVENT_GAMEOVER = 7;
    public static final int EVENT_UNDO = 8;
    public static final int EVENT_VIEW_GAMEOVER = 9;
}
