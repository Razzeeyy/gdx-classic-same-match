package com.razzeeyy.classicsamematch.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.razzeeyy.classicsamematch.ClassicSameMatchGame;

class DesktopLauncher {
    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.samples = 2;
        config.title = "Same Game";
        config.addIcon("icon128.png", Files.FileType.Internal);
        config.addIcon("icon32.png", Files.FileType.Internal);
        config.addIcon("icon16.png", Files.FileType.Internal);
        new LwjglApplication(new ClassicSameMatchGame(), config);
    }
}
